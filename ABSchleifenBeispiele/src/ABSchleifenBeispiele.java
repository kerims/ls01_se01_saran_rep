import java.util.Scanner;

public class ABSchleifenBeispiele {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl zwischen 2 und 9 ein");
		int zahl = scan.nextInt();
		gibMatrixAus(zahl);
	}
	
	public static void gibMatrixAus(int zahl) {
		for(int i = 0; i < 100; i++) {
			if(i % zahl == 0 && i != 0)
				System.out.printf("%3s", "*");
			else
				System.out.printf("%3d", i);
			if((i+1) % 10 == 0 && i != 0)
				System.out.print("\n");
		}
	}
	
}
