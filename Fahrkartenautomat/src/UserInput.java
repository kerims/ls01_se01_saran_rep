import java.util.Scanner;

public class UserInput {
	Scanner t = new Scanner(System.in);	
	
	public UserInput() {
		
	}
	
	public int[] datenErfassen(String[] bezeichnung, double[] preise) {
		System.out.println("Fahrkartenbestellvorgang:");
    	System.out.println("=========================\n");
    	int auswahl = -1;
    	boolean a = true;
	    	while(a) {
	    	System.out.println("W�hlen Sie:");
	    	for(int n = 0; n < bezeichnung.length; n++) {
	    		System.out.println("(" + n + ") " +bezeichnung[n] + ": " + preise[n] + "�");
	    	}
	    	System.out.print("\nIhre auswahl: ");
	    	auswahl = t.nextInt();
	    	if(!(auswahl >= 0 && auswahl < 10))
	    		System.out.println("Ung�ltige Auswahl. Bitte versuchen Sie es erneut");
	    	else
	    		a = false;
    	}   	
    	System.out.println("Anzahl der Tickets: ");
    	System.out.print("\nIhre auswahl: ");
    	int anzahl = t.nextInt();
    	int[] daten = {auswahl, anzahl};
   	
    	return daten;
	}
	
	public double geldEinwurf(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        
    	 eingezahlterGesamtbetrag = 0.0;
         while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
             System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
             System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
             eingeworfeneMuenze = t.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMuenze;
         }
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
}
