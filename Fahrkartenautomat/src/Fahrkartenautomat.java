
public class Fahrkartenautomat {
	private String[] bezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin Bc", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte AB", "Tageskarte BC", "Tageskarte ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
	private double[] preise = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 25.5, 24.3, 24.9};
	private UserInput ui = new UserInput();
	private Bestellung b = new Bestellung();;
	private double wechselgeld;
	
	public Fahrkartenautomat() {
	}
	
	public String[] getBezeichnung(){
    	return bezeichnung;
	}
	
	public void setBezeichnung() {
		
	}
	
	
	public double[] getPreise(){
		return preise;
	}
	
	public void setPreise() {

	}
	
	public void bestellungErfassung() {
    	int[] input = ui.datenErfassen(bezeichnung, preise);
    	b.setAnzahl(input[1]);
    	b.setBezeichnung(bezeichnung[input[0]]);
    	b.setPreis(preise[input[0]]*input[1]);
	}
	
	public void Bezahlung() {
		wechselgeld = ui.geldEinwurf(b.getPreis());
	}
	
	public void fahrkartenAusgabe() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
        	System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
	}
	
	public void rueckgeldAusgabe() {
    	if (wechselgeld > 0.0) {
            //***** Lösung der Ausgabenformatierungsaufgabe *****************/
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", wechselgeld);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (wechselgeld >= 2.0) // 2 EURO-Münzen
            {
            	muenzeAusgeben(2, "Euro");
                wechselgeld -= 2.0; 
            }
            while (wechselgeld >= 1.0) // 1 EURO-Münzen
            {
            	muenzeAusgeben(1, "Euro");
                wechselgeld -= 1.0;
            }
            while (wechselgeld >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50, "Cent");
                wechselgeld -= 0.5;
            }
            while (wechselgeld >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20, "Cent");
                wechselgeld -= 0.2;
            }
            while (wechselgeld >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10, "Cent");
                wechselgeld -= 0.1;
            }
            while (wechselgeld >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5, "Cent");
                wechselgeld -= 0.05;
            }
        }
	}
	
	
	private static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
}

