import java.util.Scanner;

class FahrkartenautomatAlt {
    public static void main(String[] args) {
        double zuZahlenderBetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();

        // Geldeinwurf
        // -----------
    
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

        // Fahrscheinausgabe
        // -----------------
        
        fahrkartenAusgeben();

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        
        rueckgeldAusgeben(rueckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	String[] bez = new String[10];
    	double[] preis = new double[10];
    	bez[0] = "Einzelfahrschein Berlin AB";
    	bez[1] = "Einzelfahrschein Berlin BC";
    	bez[2] = "Einzelfahrschein Berlin ABC";
    	bez[3] = "Kurzstrecke";
    	bez[4] = "Tageskarte AB";
    	bez[5] = "Tageskarte BC";
    	bez[6] = "Tageskarte ABC";
    	bez[7] = "Kleingruppen-Tageskarte Berlin AB";
    	bez[8] = "Kleingruppen-Tageskarte Berlin BC";
    	bez[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	preis[0] = 2.9;
    	preis[1] = 3.3;
    	preis[2] = 3.6;
    	preis[3] = 1.9;
    	preis[4] = 8.6;
    	preis[5] = 9;
    	preis[6] = 9.6;
    	preis[7] = 23.5;
       	preis[8] = 24.3;
    	preis[9] = 24.9;
    	
    	System.out.println("Fahrkartenbestellvorgang:");
    	System.out.println("=========================\n");
    	int anzahl;
    	int auswahl = -1;
    	boolean a = true;
	    	while(a) {
	    	System.out.println("W�hlen Sie:");
	    	for(int n = 0; n < bez.length; n++) {
	    		System.out.println("(" + n + ") " +bez[n] + ": " + preis[n] + "�");
	    	}
	    	System.out.print("\nIhre auswahl: ");
	    	auswahl = tastatur.nextInt();
	    	if(!(auswahl >= 0 && auswahl < 10))
	    		System.out.println("Ung�ltige Auswahl. Bitte versuchen Sie es erneut");
	    	else
	    		a = false;
    	}
    	System.out.println("Anzahl der Tickets: ");
    	System.out.print("\nIhre auswahl: ");
    	anzahl = tastatur.nextInt();
    	
    	return preis[auswahl] * anzahl;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {	
    	Scanner tastatur = new Scanner(System.in);
    	
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        
    	 eingezahlterGesamtbetrag = 0.0;
         while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
             System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
             System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
             eingeworfeneMuenze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMuenze;
         }
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
        	System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	if (rueckgabebetrag > 0.0) {
            //***** Lösung der Ausgabenformatierungsaufgabe *****************/
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
            	muenzeAusgeben(2, "Euro");
                rueckgabebetrag -= 2.0; 
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
            	muenzeAusgeben(1, "Euro");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50, "Cent");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20, "Cent");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10, "Cent");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5, "Cent");
                rueckgabebetrag -= 0.05;
            }
        }

    }
    
    public static void warte(int millisekunde) {
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    
}