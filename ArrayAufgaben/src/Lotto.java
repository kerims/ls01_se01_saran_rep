
public class Lotto {

	public static void main(String[] args) {
		int[] zahlen = new int[6];
		zahlen[0] = 3;
		zahlen[1] = 7;
		zahlen[2] = 12;
		zahlen[3] = 18;
		zahlen[4] = 37;
		zahlen[5] = 42;
		
		for(int i = 0; i < zahlen.length; i++) {
			if(i == 0)
				System.out.print("[ ");
			System.out.print(zahlen[i] + " ");
			if(i == zahlen.length - 1)
				System.out.println("]");
		}
		boolean z12 = false;
		boolean z13 = false;
		for(int i = 0; i < zahlen.length; i++) {
			if(zahlen[i] == 12)
				z12 = true;
			if(zahlen[i] == 13)
				z13 = true;
		}
		
		if(z12 == true)
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		else
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		
		if(z13 == true)
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		else
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		
	}

}
