
public class ArrayHelper {

	public static void main(String[] args) {
		int[] zahlen = new int[5];
		zahlen[0] = 1;
		zahlen[1] = 2;
		zahlen[2] = 3;
		zahlen[3] = 4;
		zahlen[4] = 5;
		
		//System.out.println(convertArrayToString(zahlen));
		//reihenfolgeUmtauschen(zahlen);
	}
	
	public static String convertArrayToString(int[] zahlen) {
		String r = Integer.toString(zahlen[0]);
		for(int i = 1; i < zahlen.length; i++) {
			r += ", " + Integer.toString(zahlen[i]);
		}
		return r;
	}
	
	public static void reihenfolgeUmtauschen(int[] zahlen) {
		int h;
		for(int i = 0; i < zahlen.length/2; i++) {
			h = zahlen[i];
			zahlen[i] = zahlen[zahlen.length - 1 - i];
			zahlen[zahlen.length - 1 - i] = h;
		}
		for(int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] + " ");
		}	
	}

}
