import java.util.Scanner;

public class Auswahlstrukturbeispiele {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie ein Jahr ein");
		int sj = scan.nextInt();
		istSchaltjahr(sj);
	}
	
	public static void kleinereZahl(int z1, int z2) {
		if(z1 < z2)
			System.out.println(z1);
		else
			System.out.println(z2);
	}

	public static void istSchaltjahr(int sj) {
		if(sj % 4 ==0) {
			if(sj % 100 == 0) {
				if(sj % 400 == 0)
					System.out.println(sj + " ist ein Schaltjahr");
				else
					System.out.println(sj + " ist kein Schaltjahr");
			}
			else
				System.out.println(sj + " ist ein Schaltjahr");
		}
	}
}
