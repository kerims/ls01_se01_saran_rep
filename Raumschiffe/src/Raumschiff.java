import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		
	}
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, 
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(this.photonentorpedoAnzahl <= 0)
			this.nachrichtAnAlle("-=*Click*=-");
		else {
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			this.treffer(r);
			this.photonentorpedoAnzahl--;
		}
	}
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(this.energieversorgungInProzent < 50)
			this.nachrichtAnAlle("-=*Click*=-");
		else {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			this.treffer(r);
		}
	}
	private void treffer(Raumschiff r){
		System.out.println(r.getSchiffsname() + " wurde getroffen!");
		r.setSchildeInProzent(r.getSchildeInProzent() - 50);
		if(r.getSchildeInProzent() <= 0) {
			r.setHuelleInProzent(r.getHuelleInProzent() - 50);
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);
			if(r.getHuelleInProzent() <= 0) {
				r.setLebenserhaltungssystemeInProzent(0);
				this.nachrichtAnAlle("Die Lebenserhaltungssysteme von " + r.schiffsname + " wurden zerstört.");
			}
		}		
	}
	public void nachrichtAnAlle(String message) {
		this.broadcastKommunikator.add(message);
	}
	public ArrayList<String> eintraegeLogbuchgZurueckgeben(){
		return this.broadcastKommunikator;
	}
	public void photonentorpedosLaden(int anzahlTorpedos) {
		for(int x = 0; x < this.ladungsverzeichnis.size(); x++) {
			if(this.ladungsverzeichnis.get(x).getBezeichnung() == "Photonentorpedo") {
				if(anzahlTorpedos > this.ladungsverzeichnis.get(x).getMenge()) {
					anzahlTorpedos = this.ladungsverzeichnis.get(x).getMenge();
				}
				this.photonentorpedoAnzahl = this.photonentorpedoAnzahl + anzahlTorpedos;
				this.ladungsverzeichnis.get(x).setMenge(this.ladungsverzeichnis.get(x).getMenge() - anzahlTorpedos);
			}
		}
	}
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		Random random = new Random();
		if(this.androidenAnzahl < anzahlDroiden)
			anzahlDroiden = this.androidenAnzahl;
		int x = 0;
		if(schutzschilde == true)
			x++;
		if(energieversorgung == true)
			x++;
		if(schiffshuelle == true)
			x++;	
		int y = random.nextInt(100) * anzahlDroiden / x;
		if(schutzschilde == true)
			this.schildeInProzent = this.schildeInProzent + y;
		if(energieversorgung == true)
			this.energieversorgungInProzent = this.energieversorgungInProzent + y;
		if(schiffshuelle == true)
			this.huelleInProzent = this.huelleInProzent + y;
	}
	public void zustandRaumschiff(){
		System.out.println("Schiffszustand: " + this.schiffsname);
		System.out.println("PhotonentorpedoAnzahl: " + this.photonentorpedoAnzahl);
		System.out.println("Energieversorgung: " + this.energieversorgungInProzent);
		System.out.println("Schilde: " + this.schildeInProzent);
		System.out.println("Hülle: " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent);
		System.out.println("Androiden Anzahl: " + this.androidenAnzahl);
	}
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsverzeichnis: " + this.schiffsname);
		for(int x = 0; x < this.ladungsverzeichnis.size(); x++)
			System.out.println("Bezeichnung: " + this.ladungsverzeichnis.get(x).getBezeichnung() + "; Menge: " + this.ladungsverzeichnis.get(x).getMenge());
	}
	public void ladungsverzeichnisAufraeumen() {
		for(int x = 0; x < this.ladungsverzeichnis.size(); x++) {
			if(this.ladungsverzeichnis.get(x).getMenge() <= 0) {
				this.ladungsverzeichnis.remove(x);
			}
	
		}
	}
}
