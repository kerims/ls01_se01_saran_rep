
public class Main {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);
		
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni,Var", 5);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);

		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reperaturDurchfuehren(true, true, true, 10);
		vulkanier.photonentorpedosLaden(100);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println(vulkanier.eintraegeLogbuchgZurueckgeben().toString());
		
	}

}
